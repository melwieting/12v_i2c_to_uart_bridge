/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "math.h"
#include "string.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define FIRMWARE_VERSION "V1.0.4"
#define TESTBYTE 0xa5

#define CMD_BUFF_SIZE 32
uint8_t uartRx; // used by UART ISR to get character
uint8_t cmdBuffer[CMD_BUFF_SIZE];
uint8_t cmdState, cmdCnt, rxTimeout;

#define TRUE 1
#define FALSE 0

#define I2C_MUX_ADDRESS 0xE0
#define I2C_CAM_ADDRESS 0x80


uint16_t recv_cnt=0;
uint8_t i2c_status;

uint8_t mux_chan;
uint8_t i2c_transmit[64], i2c_receive[64];

uint8_t uartRxBuffer[32];
volatile uint8_t uartRxState; // state of comm i2c rx
volatile uint8_t uartRxStatus;
#define CMD_BUFF_SIZE 32
uint8_t cmdBuffer[CMD_BUFF_SIZE];
uint8_t cmdState, cmdCnt;


typedef enum {firmware_version, delay1, delay2, delay3, laser_fb_delay_time, laser_en,
        dac1, dac2, dac3, 
        laser1_temp_hi, laser1_temp_low, 
        laser2_temp_hi, laser2_temp_low, 
        laser3_temp_hi, laser3_temp_low,
        diode_temp_variance,
        ad5593_reg,
        heat_monitor_period,
        adc1_int=0x81, adc2_int, adc3_int, adc4_int, adc7_int, adc_vsense, adc_vrefint,
        read_internal_adcs,
        adc1_ext, adc2_ext, adc3_ext, adc4_ext, adc5_ext, adc6_ext,
        read_ext_adcs,
        status_reg, ARM_reg,
        read_memory, laser_fb
} i2c_cmds;



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_I2C2_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
HAL_StatusTypeDef status;
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
uint32_t getUs(void) {
  uint32_t usTicks = HAL_RCC_GetSysClockFreq() / 1000000;
  register uint32_t ms, cycle_cnt;
  do {
    ms = HAL_GetTick();
    cycle_cnt = SysTick->VAL;
  } while (ms != HAL_GetTick());
  return (ms * 1000) + (usTicks * 1000 - cycle_cnt) / usTicks;
}
 
void delayUs(uint16_t micros) {
  uint32_t start = getUs();
  while (getUs()-start < (uint32_t) micros) {
    asm("nop");
  }
}

    /* i2c error
    #define HAL_I2C_ERROR_NONE       (0x00000000U)    //!< No error           //
    #define HAL_I2C_ERROR_BERR       (0x00000001U)    //!< BERR error         //
    #define HAL_I2C_ERROR_ARLO       (0x00000002U)    //!< ARLO error         //
    #define HAL_I2C_ERROR_AF         (0x00000004U)    //!< AF error           //
    #define HAL_I2C_ERROR_OVR        (0x00000008U)    //!< OVR error          //
    #define HAL_I2C_ERROR_DMA        (0x00000010U)    //!< DMA transfer error //
    #define HAL_I2C_ERROR_TIMEOUT    (0x00000020U)    //!< Timeout Error      //

    i2c state
    HAL_I2C_STATE_RESET             = 0x00U,   //!< Peripheral is not yet Initialized         //
    HAL_I2C_STATE_READY             = 0x20U,   //!< Peripheral Initialized and ready for use  //
    HAL_I2C_STATE_BUSY              = 0x24U,   //!< An internal process is ongoing            //
    HAL_I2C_STATE_BUSY_TX           = 0x21U,   //!< Data Transmission process is ongoing      //
    HAL_I2C_STATE_BUSY_RX           = 0x22U,   //!< Data Reception process is ongoing         //
    HAL_I2C_STATE_LISTEN            = 0x28U,   //!< Address Listen Mode is ongoing            //
    HAL_I2C_STATE_BUSY_TX_LISTEN    = 0x29U,   //!< Address Listen Mode and Data Transmission
                                                    process is ongoing                         //
    HAL_I2C_STATE_BUSY_RX_LISTEN    = 0x2AU,   //!< Address Listen Mode and Data Reception
                                                    process is ongoing                         //
    HAL_I2C_STATE_ABORT             = 0x60U,   //!< Abort user request ongoing                //
    HAL_I2C_STATE_TIMEOUT           = 0xA0U,   //!< Timeout state                             //
    HAL_I2C_STATE_ERROR             = 0xE0U    //!< Error                                     //
    */

uint8_t str_len(char *str) {
  uint16_t len;
  for (len=0; len<256; len++){
    if (str[len]==0) return (uint8_t)len;
  }
  return 0;
}

#define MAX_STR_SIZE 100
char out_str[MAX_STR_SIZE];
void uart_write_str(char *str) {
  uint8_t len;
  for (len=0; len<=MAX_STR_SIZE; len++) {
    if (str[len] == 0) {
      HAL_UART_Transmit_IT(&huart1, (uint8_t *)str, len);
      return;
    }
  }
}

int debug;
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  debug=0;
}

int char_cnt=0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  if (uartRxState == 0) uartRxState=1; // we have received at least 1 char
  cmdBuffer[cmdCnt++] = uartRxBuffer[0];
  if (cmdCnt >= CMD_BUFF_SIZE) cmdCnt=0; // something went wrong...
  if (cmdCnt == 5)cmdState=2;
  HAL_UART_Receive_IT(&huart1, (uint8_t *)uartRxBuffer, 1);
  char_cnt++;
}


uint8_t set_i2c_mux(uint8_t chan) {
  uint8_t cmd_error=0;

    i2c_status = HAL_I2C_Master_Transmit(&hi2c2, (uint16_t)I2C_MUX_ADDRESS, &chan, 1, 5);
    if (i2c_status != HAL_OK) {
      cmd_error = 1;
    }
  return cmd_error;
}

uint32_t i2c_xmit_cnt, i2c_recv_cnt;
uint8_t i2c_rx_buffer[64], i2c_tx_buffer[64];
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
  DBGMCU->APB1FZ |= 0x17;  // timers stop during debug pause
  DBGMCU->APB2FZ |= 0x04;
  
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_I2C2_Init();

  /* USER CODE BEGIN 2 */

  uint32_t *sn1 = (uint32_t *) 0x1FF80050;
  uint32_t *sn2 = (uint32_t *) (0x1FF80050+0x04);
  uint32_t *sn3 = (uint32_t *) (0x1FF80050+0x14);
  sprintf(out_str, "TV-GA-000034: %s: %x:%x:%x\r", FIRMWARE_VERSION, *sn1, *sn2, *sn3);
  uart_write_str(out_str);
  
  mux_chan=0x0f;
  set_i2c_mux(mux_chan); // turn on mux_channel

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  // start UART
  cmdState = 0;
  cmdCnt = 0;
  HAL_UART_Receive_IT(&huart1, (uint8_t *)&uartRx, 1);
  HAL_GPIO_WritePin(GPIOA, VCC_DIODE_EN_Pin, GPIO_PIN_SET);
  while (1)
  {    
    if (cmdState == 2) { // received an i2c packet
      //uint8_t cmd;
      cmdState=0;
      cmdCnt=0;
      if (cmdBuffer[0] == 0) {
        // invalid camera aux command
        sprintf(out_str, "FW1.0");
        uart_write_str(out_str);
      } else {
        i2c_status = HAL_I2C_Master_Transmit(&hi2c2, (uint16_t)I2C_CAM_ADDRESS, (uint8_t*)cmdBuffer, 5, 5);
        if (i2c_status == HAL_OK) {
          // transmit successful
          if (cmdBuffer[0]&0x80) {
            // sent read command, get data back
            uint16_t len=2;
            if (cmdBuffer[0] == laser_en) len=1;
            if (cmdBuffer[0] == read_internal_adcs) len=10;
            if (cmdBuffer[0] == read_ext_adcs) len=12;
            if (cmdBuffer[0] == ARM_reg) len=4;
            if (cmdBuffer[0] == read_memory) len=52;
            i2c_status = HAL_I2C_Master_Receive(&hi2c2, (uint16_t)I2C_CAM_ADDRESS, i2c_rx_buffer, len, 100);
            if (i2c_status == HAL_OK) {
              // receive successful, send data back through uart
              HAL_UART_Transmit_IT(&huart1, i2c_rx_buffer, len);
            } else {
              //cmd = HAL_I2C_GetState(&hi2c2); 
              sprintf(out_str, "E2");
              uart_write_str(out_str);
            }
          } else {
            // sent write command, get ack (which is the command byte)
            i2c_status = HAL_I2C_Master_Receive(&hi2c2, (uint16_t)I2C_CAM_ADDRESS, i2c_rx_buffer, 1, 100);
            if (i2c_status == HAL_OK) {
              // receive successful, send data back through uart
              sprintf(out_str, "R3");
              uart_write_str(out_str);
            } else {
              //cmd = HAL_I2C_GetState(&hi2c2); 
              sprintf(out_str, "E3");
              uart_write_str(out_str);
            }
          }
        } else {
          //cmd = HAL_I2C_GetState(&hi2c2); 
          sprintf(out_str, "E1");
          uart_write_str(out_str);
        }
      }
    }
    if (cmdState == 3) {
      // too long between characters, reset
      // clear out buffer & start over
      for (uint8_t i=0; i<CMD_BUFF_SIZE; i++) cmdBuffer[i]=0;
      cmdState=0;
      cmdCnt=0;
      rxTimeout=0;
    }

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C2 init function */
static void MX_I2C2_Init(void)
{

  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_SlaveConfigTypeDef sSlaveConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 500;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_OnePulse_Init(&htim2, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_TRIGGER;
  sSlaveConfig.InputTrigger = TIM_TS_ITR0;
  if (HAL_TIM_SlaveConfigSynchronization(&htim2, &sSlaveConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(VCC_DIODE_EN_GPIO_Port, VCC_DIODE_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED1_Pin|LED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : VCC_DIODE_EN_Pin */
  GPIO_InitStruct.Pin = VCC_DIODE_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(VCC_DIODE_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED1_Pin LED2_Pin */
  GPIO_InitStruct.Pin = LED1_Pin|LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
